import { Component, OnInit } from '@angular/core';
import { ListService } from './list.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [ListService]
})
export class AppComponent implements OnInit {
  title = 'demo-test';
  listResponse: any;
  isEditable:boolean;
  constructor(private listService: ListService) {}
  ngOnInit() {
    this.getListResponse();
  }

  getListResponse() {
    this.listService.getList().subscribe(
      reseponse => {
        this.listResponse = reseponse;
      },
      (err: HttpErrorResponse) => {
        this.listResponse = [];
      }
    );
  }

  editTitle(){
    this.isEditable=!this.isEditable;
  }
}
